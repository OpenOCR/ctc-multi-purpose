#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
中文简体识别
@author: chineseocr
"""
__version__='chineseocr'
from keys import Lang
from PIL import Image
from tensorflow.keras.models import load_model
ocrModel=''
import numpy as np

ocrModel = { 'chinese': 'ocr.h5'}

def load_models():
    modelDict = {}
    
    for lan in ocrModel:
        modelPath    = ocrModel[lan]
        characters   = Lang[lan.split('-')[0]]
        if characters is not None:            
            charactersS  = [' ']+list(characters)+[' ',' ']
            model = load_model(modelPath)
            charDict ={s:ind for ind,s in enumerate(charactersS)} 
            if ' ' in charDict:
               charDict[' '] =1
            modelDict[lan] =[model,charactersS,charDict]
            
    return  modelDict
            

modelDict = load_models()

def predict_prob(image,lan,useStr=None):
    ## useStr 用户字库
    
    scale = image.size[1]*1.0 / 32
    w = image.size[0] / scale
    w = int(w)
    if w<1:
        return [],[]
    image   = image.resize((w,32),Image.BILINEAR)
    
    image = (np.array(image.convert('L'))/255.0-0.5)/0.5
    tmp  =  np.zeros((32,w+32))
    tmp[:] = (128/255-0.5)/0.5
    tmp[:,16:-16] = image

        
    image = tmp.reshape((1,32,-1,1))
    
    model,charactersS,charDict =  modelDict.get(lan,[None,None,None])
    out =[' ']
    prob = [1.0]
    if model is not None:
          y_pred = model.predict(image)
          y_pred = y_pred[0][2:,]
          if useStr is not None and 'chinese' in lan:
              tmpuseStr=''
              indexList=[0]
              tmpuseStr = set(charactersS)&set(useStr)
              tmpuseStr = list(tmpuseStr)
              indexList+=[ charDict[s] for s in tmpuseStr]
              indexList+=[len(charactersS)-2,len(charactersS)-1]
              charactersS  = [' ']+list(tmpuseStr)+[' ',' ']
              y_pred=y_pred[:,indexList]
              
          prob = y_pred
          index = prob.argmax(axis=1)
          prob  = [ prob[ind,pb] for ind,pb in enumerate(index)]
          out,prob = decode_prob(index,prob,charactersS)
          
    return ''.join(out),prob

def decode_prob(t,prob,charactersS):
        length = len(t)
        char_list = []
        prob_list = []
        n = len(charactersS)
        for i in range(length):
            if t[i] not in [n-1,n-2] and (not (i > 0 and t[i - 1] == t[i])):
                        char_list.append(charactersS[t[i] ])
                        prob_list.append(prob[i])
        
        return char_list,[float(round(x,2)) for x in prob_list]
    

if __name__=='__main__':
    import time
    image = Image.open('test-img/506.jpg')
    t = time.time()
    res = predict_prob(image,lan='chinese',useStr=None)
    print(time.time()-t,res)
